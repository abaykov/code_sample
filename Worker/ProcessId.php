<?php

declare(strict_types=1);

namespace Sample\DocumentsTransportBundle\Worker;

use Ramsey\Uuid\Uuid;

/**
 * @todo Рассмотреть другой формат
 * Process ID. Identifies workers and classic run-and-die processes.
 */
class ProcessId
{
    private $id;

    public function __construct()
    {
        $this->id = (string)Uuid::uuid4();
    }

    public function getId(): string
    {
        return $this->id;
    }
}
