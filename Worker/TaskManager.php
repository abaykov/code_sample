<?php

declare(strict_types=1);

namespace Sample\DocumentsTransportBundle\Worker;

class TaskManager
{
    /**
     * @var Task[] [$name => $task]
     */
    private $tasks = [];

    /**
     * @param string $name Unique name
     * @param \DateInterval|string $interval Task run interval
     * @param \DateInterval|string|null $extraInterval Extra interval for your purposes
     *
     * @throws \Exception
     */
    public function addTask(string $name, $interval, $extraInterval = null): void
    {
        $interval = $interval instanceof \DateInterval ? $interval : new \DateInterval($interval);
        $extraInterval = $extraInterval instanceof \DateInterval || is_null($extraInterval) ? $extraInterval : new \DateInterval($extraInterval);

        $this->tasks[$name] = new Task($name, $interval, $extraInterval);
    }

    public function getTask(string $name): Task
    {
        return $this->tasks[$name];
    }

    public function getTaskExtraInterval(string $name): ?\DateInterval
    {
        return $this->getTask($name)->getExtraInterval();
    }

    public function isTaskTime(string $name): bool
    {
        return $this->getTask($name)->isTime();
    }

    public function completeTask(string $name): void
    {
        $this->getTask($name)->complete();
    }
}
