<?php

declare(strict_types=1);

namespace Sample\DocumentsTransportBundle\Worker;

trait TerminateOnErrorsThresholdTrait
{
    /**
     * @var int
     */
    private $maxErrorsCount;

    /**
     * @var int
     */
    private $clearErrorsTimeout;

    /**
     * @var int
     */
    private $errorsCount = 0;

    /**
     * @var int
     */
    private $firstErrorTime;

    /**
     * Call this function on error
     */
    private function increaseErrorsCount(): void
    {
        $time = time();

        if (!$this->firstErrorTime || (($time - $this->firstErrorTime) >= $this->clearErrorsTimeout)) {
            $this->errorsCount = 1;
            $this->firstErrorTime = $time;
        }

        $this->errorsCount++;
    }

    /**
     * Call this function to check errors threshold and terminate the process
     *
     * @param callable|null $callback Actions tha must be executed before exit
     */
    private function terminateOnErrorsThreshold(callable $callback = null)
    {
        if (($this->maxErrorsCount > 0) && ($this->errorsCount >= $this->maxErrorsCount)) {
            if (null !== $callback) {
                $callback();
            }

            exit('Terminated after reaching errors threshold');
        }
    }
}
