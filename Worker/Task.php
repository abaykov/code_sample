<?php

declare(strict_types=1);

namespace Sample\DocumentsTransportBundle\Worker;

class Task
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateInterval
     */
    private $interval;

    /**
     * @var \DateInterval|null
     */
    private $extraInterval;

    /**
     * @var \DateTime|null
     */
    private $completedAt;

    public function __construct(string $name, \DateInterval $interval, ?\DateInterval $extraInterval = null)
    {
        $this->name = $name;
        $this->interval = $interval;
        $this->extraInterval = $extraInterval;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getInterval(): \DateInterval
    {
        return $this->interval;
    }

    public function getExtraInterval(): ?\DateInterval
    {
        return $this->extraInterval;
    }

    public function getCompletedAt(): ?\DateTime
    {
        return $this->completedAt;
    }

    public function isTime(): bool
    {
        $taskTime = new \DateTime();
        $taskTime->sub($this->getInterval());

        return $taskTime > $this->getCompletedAt();
    }

    public function complete(): void
    {
        $this->completedAt = new \DateTime();
    }
}