<?php

declare(strict_types=1);

namespace Sample\DocumentsTransportBundle\Transport\Exception;

class TransportConnectionException extends TransportException
{
}
