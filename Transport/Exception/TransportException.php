<?php

declare(strict_types=1);

namespace Sample\DocumentsTransportBundle\Transport\Exception;

use Sample\DocumentsTransportBundle\Exception\DocumentTransportException;

class TransportException extends DocumentTransportException
{
}
