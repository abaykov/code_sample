<?php

declare(strict_types=1);

namespace Sample\DocumentsTransportBundle\Transport;

use Psr\Log\LoggerInterface;

/**
 * Memory transport system for unit-tests usage
 */
class MemoryTransport implements Transport
{
    /**
     * @var array
     */
    private $storage = [];

    /**
     * @var string
     */
    private $name;

    public function __construct(LoggerInterface $logger, string $name, array $parameters = [])
    {
        $this->name = $name;
    }

    public function getType(): string
    {
        return 'memory';
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function receive(string $target): ?string
    {
        if (!isset($this->storage[$target])) {
            return null;
        }

        return array_shift($this->storage[$target]);
    }

    public function send(string $target, string $message): void
    {
        $this->storage[$target][] = $message;
    }

    public function getCountMessages(string $target): int
    {
        return count($this->storage[$target] ?? []);
    }
}
