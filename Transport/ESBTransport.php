<?php

declare(strict_types=1);

namespace Sample\DocumentsTransportBundle\Transport;

use Sample\DocumentsTransportBundle\Transport\Exception\InvalidTransportConfigurationException;
use Sample\DocumentsTransportBundle\Transport\Exception\TransportConnectionException;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

class ESBTransport implements Transport, TransactionAwareTransport, ConnectionAwareTransport
{
    private const LOG_LABEL = '[ESBTransport] ';

    public const PARAMETERS_KEYS = [
        'host',
        'port',
        'login',
        'password',
        'use_transactions',
        'reconnect_timeout',
    ];

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \Stomp
     * @see http://php.net/manual/en/book.stomp.php
     */
    private $connectionReceiver;

    /**
     * @var \Stomp
     * @see http://php.net/manual/en/book.stomp.php
     */
    private $connectionSender;

    // -------  Temp vars (transactional) ------------

    /**
     * @var \StompFrame|null
     */
    private $receivedMessage;

    /**
     * @var string|null
     */
    private $receivedTransactionId;

    // ------------ Transport options ------------

    /**
     * @var string
     */
    private $connectionString;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @var bool
     */
    private $useTransactions = true;

    /**
     * @var int
     */
    private $reconnectTimeout = 60;

    /**
     * @var
     */
    private $lastReconnectTimestamp;

    public function __construct(LoggerInterface $logger, string $name, array $parameters = [])
    {
        $this->logger = $logger;
        $this->name = $name;
        $this->lastReconnectTimestamp = time();

        $parameters['use_transactions'] = $parameters['use_transactions'] ?? true;
        $parameters['reconnect_timeout'] = $parameters['reconnect_timeout'] ?? 60;

        $paramKeys = array_keys($parameters);

        foreach (self::PARAMETERS_KEYS as $key) {
            if (!in_array($key, $paramKeys, true)) {
                throw new InvalidTransportConfigurationException(
                    sprintf(
                        'Undefined parameter "%s". Expected params: %s',
                        $key,
                        implode(',', self::PARAMETERS_KEYS)
                    )
                );
            }
        }

        $this->connectionString = sprintf('tcp://%s:%s', $parameters['host'], $parameters['port']);
        $this->login = (string)$parameters['login'];
        $this->password = (string)$parameters['password'];
        $this->useTransactions = (bool)$parameters['use_transactions'];
        $this->reconnectTimeout = (int)$parameters['reconnect_timeout'];
    }

    public function getType(): string
    {
        return 'esb';
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Send message to target
     *
     * @param string $target The target where you want to send the document
     * @param string $message Document body (xml, json. etc...)
     *
     * @throws TransportConnectionException
     */
    public function send(string $target, string $message): void
    {
        try {
            $this->reconnectOnTimeout();

            $connection = $this->getConnectionSender();
            $result = $connection->send($target, $message);

            if (false === $result) {
                throw new TransportConnectionException(
                    sprintf('Error send message: %s', $connection->error())
                );
            }

            $this->logger->debug(
                sprintf(self::LOG_LABEL . 'Sent message to queue "%s"', $target),
                [
                    'target' => $target,
                    'message' => $message
                ]
            );
        } catch (\Throwable $exception) {
            $message = sprintf(self::LOG_LABEL . 'Transport "%s" error: %s', $this->getName(), $exception->getMessage());
            $this->logger->error($message, [
                'transport_target' => $target,
                'transport_name' => $this->getName(),
                'exception' => $exception->getTraceAsString()
            ]);

            throw new TransportConnectionException($message, $exception->getCode(), $exception);
        }
    }

    /**
     * Read message from target
     *
     * @param string $target
     * @return null|string
     *
     * @throws TransportConnectionException
     */
    public function receive(string $target): ?string
    {
        try {
            $this->reconnectOnTimeout();

            if (null === $this->connectionReceiver) {
                $this->getConnectionReceiver()->subscribe(
                    $target,
                    ['ack' => 'client-individual']
                );

                $this->logger->debug(sprintf(self::LOG_LABEL . 'Started listening queue "%s"', $target));
            }

            $connection = $this->getConnectionReceiver();

            if ($connection->hasFrame() === false) {
                return null;
            }

            $this->startTransaction();
            $this->receivedMessage = $connection->readFrame();

            if ($this->receivedMessage === null) {
                $this->logger->notice(self::LOG_LABEL . 'Received NULL-message');
                $this->abort();

                return null;
            }

            $this->logger->debug(sprintf(self::LOG_LABEL . 'Received new message (id: %s) from queue "%s"', $this->receivedMessage->headers['message-id'], $target), [
                'queue_name' => $target,
                'esb_transaction_id' => $this->receivedTransactionId
            ]);

            return $this->receivedMessage->body;
        } catch (\Throwable $exception) {
            $this->abort();

            $message = sprintf(self::LOG_LABEL . 'Transport "%s" error: %s', $this->getName(), $exception->getMessage());
            $this->logger->error($message, [
                'transport_target' => $target,
                'transport_name' => $this->getName(),
                'exception' => $exception->getTraceAsString()
            ]);

            throw new TransportConnectionException($message, $exception->getCode(), $exception);
        }
    }

    /**
     * Start transaction
     */
    private function startTransaction(): void
    {
        if (false === $this->useTransactions) {
            return;
        }

        $this->receivedTransactionId = (string)Uuid::uuid4();

        $connection = $this->getConnectionReceiver();
        $connection->begin($this->receivedTransactionId);

        $this->logger->debug(sprintf(self::LOG_LABEL . 'Start transaction "%s"', $this->receivedTransactionId));
    }

    /**
     * Commit transaction
     */
    public function commit(): void
    {
        $connection = $this->getConnectionReceiver();
        $connection->ack($this->receivedMessage);

        if (false === $this->useTransactions) {
            return;
        }

        if (null === $this->receivedTransactionId) {
            return;
        }

        if ($this->receivedTransactionId) {
            $connection->commit($this->receivedTransactionId);
            $this->logger->debug(sprintf(self::LOG_LABEL . 'Commit transaction "%s"', $this->receivedTransactionId));
        }

        $this->receivedTransactionId = null;
    }

    /**
     * Rollback transaction
     */
    public function abort(): void
    {
        if (false === $this->useTransactions) {
            return;
        }

        if ($this->receivedTransactionId) {
            $connection = $this->getConnectionReceiver();
            $connection->abort($this->receivedTransactionId);
            $this->logger->debug(sprintf(self::LOG_LABEL . 'Rollback transaction "%s"', $this->receivedTransactionId));
        }
    }

    public function connect(): void
    {
        // disabled. All methods same control connect
    }

    public function disconnect(): void
    {
        $this->connectionReceiver = null;
        $this->connectionSender = null;
    }

    /**
     * Connection for receive message
     *
     * @return \Stomp
     */
    private function getConnectionReceiver(): \Stomp
    {
        if (null === $this->connectionReceiver) {
            $this->connectionReceiver = new \Stomp(
                $this->connectionString,
                $this->login,
                $this->password
            );
        }

        return $this->connectionReceiver;
    }

    /**
     * Connection for send messages
     *
     * @return \Stomp
     */
    private function getConnectionSender(): \Stomp
    {
        if (null === $this->connectionSender) {
            $this->connectionSender = new \Stomp(
                $this->connectionString,
                $this->login,
                $this->password
            );
        }

        return $this->connectionSender;
    }

    private function reconnectOnTimeout(): void
    {
        if (($this->reconnectTimeout > 0) && ($this->lastReconnectTimestamp + $this->reconnectTimeout < time())) {
            $this->disconnect();
            $this->lastReconnectTimestamp = time();

            $this->logger->debug(sprintf(self::LOG_LABEL . 'Reconnect transport "%s" on timeout', $this->name));
        }
    }
}
