<?php

declare(strict_types=1);

namespace Sample\DocumentsTransportBundle\Transport;

interface TransactionAwareTransport
{
    /**
     * Commit transaction
     */
    public function commit(): void;

    /**
     *  Rollback transaction
     */
    public function abort(): void;
}
