<?php

declare(strict_types=1);

namespace Sample\DocumentsTransportBundle\Transport;

use Sample\DocumentsTransportBundle\Transport\Exception\TransportException;
use Psr\Log\LoggerInterface;

/**
 * Transport layer
 */
interface Transport
{
    /**
     * @param LoggerInterface $logger
     * @param string $name
     * @param array $parameters
     */
    public function __construct(LoggerInterface $logger, string $name, array $parameters);

    /**
     * Transport name
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Transport type: esb, memory, etc
     *
     * @return string
     */
    public function getType(): string;

    /**
     * Send message to target
     *
     * @param string $target The target where you want to send the document
     * @param string $message Document body (xml, json. etc...)
     *
     * @throws TransportException
     */
    public function send(string $target, string $message): void;

    /**
     * Read message from target
     *
     * @param string $target The target from which to extract the document (ex. queue_name)
     *
     * @return null|string Document body (xml, json, etc...)
     */
    public function receive(string $target): ?string;
}
