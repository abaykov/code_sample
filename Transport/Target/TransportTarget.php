<?php

declare(strict_types=1);

namespace Sample\DocumentsTransportBundle\Transport\Target;

class TransportTarget
{
    /**
     * @var string
     */
    private $transportName;

    /**
     * @var string
     */
    private $target;

    public function __construct(string $transportName, string $target)
    {
        $this->transportName = $transportName;
        $this->target = $target;
    }

    public function getTransportName(): string
    {
        return $this->transportName;
    }

    public function getTarget(): string
    {
        return $this->target;
    }
}
