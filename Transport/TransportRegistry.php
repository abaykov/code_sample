<?php

declare(strict_types=1);

namespace Sample\DocumentsTransportBundle\Transport;

use Sample\DocumentsTransportBundle\Exception\DocumentTransportException;
use Psr\Log\LoggerInterface;

class TransportRegistry
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Transport[] [$name => $transport]
     */
    private $transports;

    public function __construct(LoggerInterface $logger, array $transportSettings)
    {
        $this->logger = $logger;

        $supportedTypes = self::getSupportedTypes();

        foreach ($transportSettings as $name => $settings) {
            $transportName = $name;
            $transportType = $settings['type'];
            $transportParameters = $settings['parameters'];

            if (!in_array($transportType, array_keys($supportedTypes), true)) {
                throw new DocumentTransportException(sprintf(
                    'Unknown transport type - "%s". Allowed: %s',
                    $transportType,
                    implode(', ', array_keys($supportedTypes))
                ));
            }

            $this->transports[$transportName] = new $supportedTypes[$transportType]($logger, $transportName, $transportParameters);
        }
    }

    public static function getSupportedTypes(): array
    {
        return [
            'esb' => ESBTransport::class,
            'memory' => MemoryTransport::class
        ];
    }

    /**
     * @param string $name
     *
     * @return Transport
     *
     * @throws DocumentTransportException
     */
    public function getTransport(string $name): Transport
    {
        if (!isset($this->transports[$name])) {
            throw new DocumentTransportException(sprintf(
                'Unknown transport name - "%s". Allowed: %s',
                $name,
                implode(', ', array_keys($this->transports))
            ));
        }

        return $this->transports[$name];
    }
}
