<?php

declare(strict_types=1);

namespace Sample\DocumentsTransportBundle\Transport;

interface ConnectionAwareTransport
{
    public function connect(): void;

    public function disconnect(): void;
}
